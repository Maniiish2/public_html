<?php defined('BASEPATH') OR exit('No direct script access allowed');


class College_model extends CI_Model{

function add_college($data){
    $this->db->insert('college', $data);
    return true;

}

function view_college(){
    return $this->db->select('*')->from('college')->get()->result();
}

function view_university(){
    return $this->db->select('*')->from('university')->get()->result();
}


function edit($id){
    return $this->db->select('*')->from('college')->where('id',$id)->get()->row();
}

function update($data,$id){
    return $this->db->where('id',$id)->update('college',$data);
}


function get_college_by_univ_id($id)
{
    return $this->db->select('*')->from('college')->where('id',$id)->get->result();
}

}



?>