<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Apply_now_model extends CI_Model{


  public function get_applied_data()
  {
        
        $query=$this->db->query("select * from apply_now");
	       return $query->result();
  }

  function standard()
  {
      return $this->db->select('*')->from('standards')->get()->result();
  }


  
  function view()
  {

 
       $query =  $this->db->query(
        'SELECT 
        apply_now.*,
        university.university,
        college.col_name,
        Webinar.Webinar,
        course.course,
        standards.standard_name
  
        FROM apply_now 
        Inner JOIN university ON apply_now.university = university.id 
        INNER JOIN college ON apply_now.college = college.id 
        LEFT JOIN Webinar ON apply_now.Webinar = Webinar.id
        LEFT JOIN course ON apply_now.course = course.id
        Inner JOIN standards ON apply_now.standard = standards.standard_id
        order by apply_now.apply_id ASC
                
        ');
       

    return $query->result();


    /*
    
      $query =  $this->db->query(
       'SELECT 
       apply_now.*,
       university.university,
       college.col_name,
       Webinar.Webinar,
       standards.standard_name
 
       FROM apply_now 
       Inner JOIN university ON apply_now.university = university.id 
       INNER JOIN college ON apply_now.college = college.id 
       Inner JOIN Webinar ON apply_now.Webinar = Webinar.id
       Inner JOIN standards ON apply_now.standard = standards.standard_id
       ');
    

 */

  }

    public function view_array()
    {

      $type1=$this->session->userdata('user_search_type1');
      $from1=date("Y-m-d", strtotime($this->session->userdata('user_search_from1')));
      $to1=date("Y-m-d", strtotime($this->session->userdata('user_search_to1')));

  
        $query =  $this->db->query(
          'SELECT 
          apply_now.*,
          university.university,
          college.col_name,
          Webinar.Webinar,
          course.course,
          standards.standard_name

         
          FROM apply_now 
          Inner JOIN university ON apply_now.university = university.id 
          INNER JOIN college ON apply_now.college = college.id 
          LEFT JOIN Webinar ON apply_now.Webinar = Webinar.id
          LEFT JOIN course ON apply_now.course = course.id
          Inner JOIN standards ON apply_now.standard = standards.standard_id

          order by apply_now.apply_id ASC

                  
          ');
          // if($this->session->userdata('user_search_type1')!='')
          // $this->db->where('is_active',$this->session->userdata('user_search_type1'));
        
          // if($this->session->userdata('user_search_from1')!='')
          // $this->db->where(' submitted_at  >= ',date('Y-m-d', strtotime($this->session->userdata('user_search_from1'))));
    
          // if($this->session->userdata('user_search_to1')!='')
          // $this->db->where(' submitted_at  <= ',date('Y-m-d', strtotime($this->session->userdata('user_search_to1'))));
    
          // IF($type1 !="",where("is_active",$type1),"1");

          /* 
          not working code
           
          CASE WHEN  $type1 !="" THEN where("is_active",$type1)
          WHEN $from1 !="" THEN where("submitted_at  >= ",$from1)
          WHEN $to1   !="" THEN where("submitted_at  <= ",$to1)
          ELSE else_result END

    
          
          */

      return $query->result_array();
    }



		//---------------------------------------------------
		// get all users for server-side datatable with advanced search
		public function get_all_users_by_applyform(){

			$this->db->select('*');

			if($this->session->userdata('user_search_type1')!='')
			$this->db->where('is_active',$this->session->userdata('user_search_type1'));
		
			if($this->session->userdata('user_search_from1')!='')
			$this->db->where(' submitted_at  >= ',date('Y-m-d', strtotime($this->session->userdata('user_search_from1'))));

			if($this->session->userdata('user_search_to1')!='')
			$this->db->where(' submitted_at  <= ',date('Y-m-d', strtotime($this->session->userdata('user_search_to1'))));

			return $this->db->get('apply_now')->result_array();
		}





}

?>