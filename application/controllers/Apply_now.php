<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apply_now extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model('Apply_now_model');
        $this->load->model('admin/Course_model','Course_model');


	}




        public function index()
        {


            $data['view_uni']=$this->Course_model->view_university();
            $data['view_course']=$this->Course_model->view_course();
            $data['view_Webinar']=$this->Course_model->view_Webinar();
            $data['view_college']=$this->Course_model->view_college();

            $data['standard']=$this->Apply_now_model->standard();


        
            $this->load->view('front/include/header',$data);
            $this->load->view('front/applynow',$data);
            $this->load->view('front/include/footer');

        } 

     

        public function applyform()
        {

            // print_r($this->input->post());
            // exit;
            $captcha_response = trim($this->input->post('g-recaptcha-response'));
        
            if(!empty($captcha_response))
            { 
            
                $this->form_validation->set_rules('courses','Course-Name', 'trim');
                $this->form_validation->set_rules('Webinar','Webinar-Name', 'trim');
                $this->form_validation->set_rules('univ_id', 'University', 'trim|required');
                $this->form_validation->set_rules('name', 'Name', 'trim|required|alpha_numeric_spaces');
                $this->form_validation->set_rules('phone', 'Phone', 'trim|required|is_natural|max_length[12]');
                $this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
                $this->form_validation->set_rules('standard', 'Current Standard', 'trim|required');
                $this->form_validation->set_rules('col_id', ' Name of College ', 'trim|required');
                $this->form_validation->set_rules('sex', 'Gender', 'trim|required');
                $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required');
                $this->form_validation->set_rules('payment', 'Payment', 'trim|required');
                $this->form_validation->set_rules('scholarship', 'Scholarship', 'trim|required');
                $this->form_validation->set_rules('message', 'Message', 'trim');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

                if ($this->form_validation->run() == FALSE) {                
            
                    $data = array(
                        'errors' => validation_errors()
                    );
                    
                    $this->session->set_flashdata('form_data', $this->input->post());
                    $this->session->set_flashdata('error', $data['errors']);
                    $this->flasher->error($data['errors']);

                    redirect('Apply_now', 'refresh');
                    // echo "<script> window.location.href = window.location.href;</scirpt>";

                }else{

                    if (!empty($captcha_response)) {

                        $keySecret = '6LeHo3kaAAAAADms4C8Ra3oMhRdUSP8Qdz0zh0xZ';
    
                        $check = array(
                        'secret'		=>	$keySecret,
                        'response'		=>	$captcha_response,
                    );
    
                        $startProcess = curl_init();
    
                        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
    
                        curl_setopt($startProcess, CURLOPT_POST, true);
    
                        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
    
                        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
    
                        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
    
                        $receiveData = curl_exec($startProcess);
    
                        $finalResponse = json_decode($receiveData, true);

                        if ($finalResponse['success']) {
                            $contactData = array(
                        'apply_type'                => $this->input->post('apply_type', true),
                        'course'                => $this->input->post('courses', true),
                        'webinar'               => $this->input->post('webinar', true),
                        'university'            => $this->input->post('univ_id', true),
                        'name'                  => $this->input->post('name', true),
                        'email'                 => $this->input->post('email', true),
                        'phone'                 => $this->input->post('phone', true),
                        'standard'              => $this->input->post('standard', true),
                        'college'               => $this->input->post('col_id', true),
                        'sex'                   => $this->input->post('sex', true),
                        'dob'                   => $this->input->post('dob', true),
                        'payment'               => $this->input->post('payment', true),
                        'scholarship'           => $this->input->post('scholarship', true),
                        'message'               => $this->input->post('message', true)
                    );

                            $result  = $this->Apply_now_model->apply_now_insert($contactData);
                            if ($result) {
                                $this->session->set_flashdata('success', 'Thank you! We Will Contact You Soon!!!!');
                                redirect('Apply_now', 'refresh');
                                //echo "<script> window.location.href = window.location.href;</scirpt>";
                            }
                        } else {
                            $this->session->set_flashdata('error', 'Validation Fail Try Again');
                            redirect('Apply_now', 'refresh');
                                ///echo "<script> window.location.href = window.location.href;</scirpt>";
                        }

                    }else{
                       // $this->flasher->error('Captcha Error!');
                       $this->session->set_flashdata('error', 'Validation Fail Try Again');

                        redirect('Apply_now', 'refresh');

                        //redirect(base_url('Apply_now'));
                        //echo "<script> window.location.href = window.location.href;</scirpt>";


                    }

            }

        }else{   

            $this->flasher->error('Captcha Error!');
            //$this->flasher->info('Object Info');
            
            // Flasher::addSuccess('Static Success');
            // Flasher::addWarning('Static Warning');
            // Flasher::addError('Static Error');
            
            
            $this->session->set_tempdata('captcha_error', 'Captcha Not Click');
            redirect(base_url('Apply_now', 'refresh'));

            // $data['view_uni']=$this->Course_model->view_university();
            // $data['view_course']=$this->Course_model->view_course();
            // $data['view_Webinar']=$this->Course_model->view_Webinar();
            // $data['view_college']=$this->Course_model->view_college();
            
            // $data['standard']=$this->Apply_now_model->standard();


        
            // $this->load->view('front/include/header');
            // $this->load->view('front/applynow',$data);
            // $this->load->view('front/include/footer');
            

     }
  }

    function check_default($array)
    {
        if(!empty($array)) 
        {
            foreach($array as $element)
            {
                if($element == '0')
                { 
                return FALSE;
                }
            }
        }else{
            return TRUE;
        }
    }

    function view_College(){


		$states = $this->db->select('*')->where('univ_id',$this->input->post('univ_id'))->get('college')->result_array();
		$options = array('' => 'Select Option') + array_column($states,'col_name','id');
		$html = form_dropdown('col_id',$options,'','class="form-control select2" required');
		$error =  array('msg' => $html);
	    //echo json_encode($error);
	    // echo json_encode($states);

	//	print_r($this->input->post('univ_id'));



    
		foreach($states as $row){

			echo '<option value="'.$row['id'].'">'.$row['col_name'].'</option>';		
        
        }

	}

     
}
