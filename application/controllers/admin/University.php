<?php defined('BASEPATH') OR exit('No direct script access allowed');
class University extends my_Controller{


	function __construct(){
		parent::__construct();
		auth_check(); // check login auth

		$this->rbac->check_module_access();
		$this->load->model('admin/Course_model','add_university');

	}

    function index (){

		
        $data['title'] = 'University List';
        $data['result']=$this->add_university->view_university();
      
      
              $this->load->view('admin/includes/_header');
              $this->load->view('admin/course/view_university', $data);
              $this->load->view('admin/includes/_footer');

  }
    
      
function add_university(){

	if ($this->input->post('university')) {
	   
		$this->form_validation->set_rules('university', 'University', 'trim|is_unique[university.university]|required');
  
  
  
		if ($this->form_validation->run() == FALSE) {
					  $data = array(
						  'errors' => validation_errors()
					  );
					  $this->session->set_flashdata('form_data', $this->input->post());
					  $this->session->set_flashdata('errors', $data['errors']);
					  redirect(base_url('admin/Course/add_courses'),'refresh');
				  }else{
					  $result=$this->add_university->add_university($this->input->post());
  
					  if ($result) {
						  $this->session->set_flashdata('success', 'University added successfully');	
						  redirect(base_url('admin/Course/add_university', 'refresh'));
					  }
				  }
	}else{
  
	  $data['title'] = 'Add University';
  
	$data['university']= $this->add_university->view_university();
  
	$this->load->view('admin/includes/_header');
	$this->load->view('admin/course/add_university', $data);
	$this->load->view('admin/includes/_footer');
	}
  }

  function edit($id)
  {
	

	$data['title'] = 'University List';
	$data['result']=$this->add_university->edit_university($id);

  
		  $this->load->view('admin/includes/_header');
		  $this->load->view('admin/course/edit_university', $data);
		  $this->load->view('admin/includes/_footer');

  }

  function update()
  {

	$id=$this->input->post('id');
	if($this->input->post('university'))
	{
			$update_rows = array('university' => $this->input->post('university'));

			$result=$this->add_university->update_university($update_rows,$id);
			if($result)
			{
				$this->session->set_flashdata('success', 'University updated successfully');	
				redirect(base_url('admin/University'));
				echo '<script>location = self.location.href</script>';
			}

	}
  
  }



function delete($id)
 {
   
	   $this->db->where('id',$id);
	  
	   return $ci->db->delete('university');
  
  
  }
  

    }

    
