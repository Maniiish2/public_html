<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Apply_now_records extends MY_Controller {

	function __construct(){
		parent::__construct();
		auth_check(); // check login auth

		$this->rbac->check_module_access();
        $this->load->model('admin/Apply_now_model','admin_appy');
        

	}


    function index()
    {


      $data['results'] = $this->admin_appy->view();
        
        $this->load->view('admin/includes/_header');

        $this->load->view('admin/apply_now_view', $data);

        $this->load->view('admin/includes/_footer');

    }

    function standard()
    {

        $data['result']=$this->admin_appy->standard();

        $this->load->view('admin/includes/_header');

        $this->load->view('admin/standard', $data);

        $this->load->view('admin/includes/_footer');



    }
    function advanced_apply_now()
    {
      
        $data['result'] = $this->admin_appy->view_array();


		$data['title'] = 'Advanced Option on Apply Now Data';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/advance_search', $data);
		$this->load->view('admin/includes/_footer');


    }
    // Advanced Search Example
	public function advance_search(){

		$this->session->unset_userdata('user_search_type1');
		$this->session->unset_userdata('user_search_from1');
		$this->session->unset_userdata('user_search_to1');

		$data['title'] = 'Advanced Search with Apply Now';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/advance_search', $data);
		$this->load->view('admin/includes/_footer');
	}

	//-------------------------------------------------------


    function search(){

        

		$this->session->set_userdata('user_search_type1',$this->input->post('user_search_type1'));
		$this->session->set_userdata('user_search_from1',$this->input->post('user_search_from1'));
		$this->session->set_userdata('user_search_to1',$this->input->post('user_search_to1'));
	}
    //---------------------------------------------------
	
    
    // Server-side processing Datatable Example with Advance Search
	public function advance_datatable_json(){	

		$records['data'] = $this->admin_appy->view_array();
		$data = array();
		$i=0;
		foreach ($records['data']  as $row) 
		{  
			$status = ($row['is_active'] == 1)? 'checked': '';
			$data[]= array(
				++$i,
				   $row['name'],
                   $row['email'],
                   $row['phone'],
                   $row['Webinar'],
                   $row['university'],
                   $row['standard_name'],
                   $row['col_name'],
                   
				   date_time($row['submitted_at']),	
				'<input type="checkbox" class="tgl-ios" '.$status.'><label for=""></label>'
			);
		}
		$records['data'] = $data;
		echo json_encode($records);						   
	}



    //---------------------------------------------------------------
	//  Export Users PDF 
	public function apply_pdf(){


		$this->load->helper('pdf_helper'); // loaded pdf helper
        $data['result'] = $this->admin_appy->view_array();

		$this->load->view('admin/apply_now_pdf', $data);
	}

    


    //---------------------------------------------------------------	
	// Export data in CSV format 
	public function export_csv(){ 

        // file name 
         $filename = 'applynow_'.date('Y-m-d').'.csv'; 
         header("Content-Description: File Transfer"); 
         header("Content-Disposition: attachment; filename=$filename"); 
         header("Content-Type: application/csv; ");
 
        // get data 
         // $user_data = $this->example_model->get_users_for_csv();

           $user_data = $this->admin_appy->view_array();

        //   echo "<pre>";

        //   print_r($user_data);

        //   exit;

 
        // file creation 
         $file = fopen('php://output', 'w');
 
         $header = array("Apply ID", "Apply Type", "Course", "webinar id", "University", "name", "email", "phone", "standard", "college", "sex", "dob", "payment", "scholarship","Message", "Created Date", "is_active", "college name", "Webinar Name", "Standard Name"); 
         fputcsv($file, $header);
         foreach ($user_data as $key=>$line){ 
             fputcsv($file,$line); 
         }
         fclose($file); 
         exit; 
     }
 
    


}

?>
