<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$this->load->model('Apply_now_model');
        $this->load->model('admin/Course_model','Course_model');

		//redirect(base_url('admin'));

		$data['view_uni']=$this->Course_model->view_university();
		$data['view_course']=$this->Course_model->view_course();
		$data['view_Webinar']=$this->Course_model->view_Webinar();
		$data['view_college']=$this->Course_model->view_college();

		$data['standard']=$this->Apply_now_model->standard();


		$this->load->view('front/include/header',$data);
		$this->load->view('front/index',$data);
		$this->load->view('front/include/footer');

		
	}

	public function site_lang($site_lang){
		echo $site_lang;
		echo '<br>';
		echo 'you will be redirected to :'.$_SERVER['HTTP_REFERER'];
		$language_data = array(
			'site_lang' => $site_lang
		);

		$this->session->set_userdata($language_data);

		if ($this->session->userdata('site_lang')) {
			echo 'user session language is = '.$this->session->userdata('site_lang');
		}
		redirect($_SERVER['HTTP_REFERER']);

		exit;
	}

	public function auth(){

		redirect(base_url('Auth/login'));

				$this->load->view('front/include/header');
				$this->load->view('front/front-auth');
				$this->load->view('front/include/footer');
	}

	function hello(){

		echo "manish testing";
	}
	

	
}
