<style>

</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" /> -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<!--     <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2.9.2/dist/parsley.min.js"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/parsleyjs@2/dist/parsley.min.js"></script>
    
    
    
    

<div class="limiter">
  <div class="container-login100">
    <div class=" Apply_noy">
    <form id="demo-form" data-parsley-validate="" action="<?=base_url('Apply_now/applyform')?>" method="post">
          <span class="apply_logo">
        <img src="<?=base_url()?>public/assets/img/logo-dark.png" alt="logo">
      </span>

      <br>
      <div class="app-web text-center" style="width: 100%;">
		
      <label class="form-check-label f_uncheck" for="w1">
        <input type="radio" value='1' class="form-check-input " id="w1" required=""  name="apply_type" >   
         <strong style="margin-left: 17px; font-size: 16px;">Course</strong>  
      </label>
 
                      
          <label class="form-check-label f_uncheck current" for="w2" style="margin-right: 10px">
             <input type="radio" value='2' class="form-check-input" id="w2" required=""  name="apply_type" checked>  
              <strong class="w_text" style="margin-left: 17px;font-size: 16px;">Webinar</strong> 
          </label>
                      
					    
					</div>

          <div class="bs-callout bs-callout-warning hidden">
            <h4>Oh snap!</h4>
            <p>         
            <?php $this->load->view('admin/includes/_messages.php') ?>
            <?php $this->load->view('partials/flash_messages/bootstrap') ?>
            <?php echo validation_errors('<div class="error">', '</div>'); ?>

          </p>
         </div>

          <div class="bs-callout bs-callout-info hidden">
            <h4>Yay!</h4>
            <p>Everything seems to be ok :)</p>
         </div>


      <span class="apply_logo bs-callout-info bs-callout ">
        <div>
        <?php  $this->load->view('admin/includes/_messages.php') ?>

         <?php  $this->load->view('partials/flash_messages/bootstrap') ?>
         <?php  echo validation_errors('<div class="error">', '</div>'); ?>
        </div>
      </span>

      
  



      <div class="form_inputfff">
    
<!-- START APPLICATION TYPE -->

      <div class="col-md-6">
        <div class="web1"  style="display: none;">
          <label for="staticEmail" class=" col-form-label">Course Applied For 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
          <select  class="input100" required=""  name="courses" id='courses' >

          <option value='0'>--Select Anyone--</option>
                     
         <?php if(!empty($view_course)):
              foreach($view_course as $v_course):?>
                <option value='<?=$v_course->id;?>'>
                  <?=$v_course->course;?>
                </option>
         <?php endforeach; endif;?>
       </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <div class="web2">
          <label for="staticEmail" class=" col-form-label">Webinar Applied For 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required=""  name="webinar" id='webinar'>
            <option value="0">Choose..</option>
             
            <?php if(!empty($view_Webinar)):
              foreach($view_Webinar as $v_Webinar):?>
                <option value='<?=$v_Webinar->id;?>'>
                  <?=$v_Webinar->webinar;?>
                </option>
               <?php endforeach; endif;?>
          </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
      </div>


<!-- END APPLICATION TYPE -->




        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">University
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required=""  data-parsley-trigger="keyup" name="univ_id" id='univ_id'  value="<?php echo set_value('univ_id'); ?>" >

            <option value="0">Choose..</option>

        
            <?php if(!empty($view_uni)):
			       	foreach($view_uni as $v_uni):?>
                 <option value='<?=$v_uni->id;?>'>
                  <?=$v_uni->university;?>
                </option>
              <?php endforeach; endif;?>
            </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
        
        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Current Standard 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required="" name="standard" id='standard' value="<?php echo set_value('standard'); ?>" >
            <option value="0">Choose..</option>
            
              
            <?php if(!empty($standard)):

            foreach($standard as $stand):?>
              <option value='<?=$stand->standard_id;?>'>
                <?=$stand->standard_name;?>
              </option>
            <?php endforeach; endif;?>
              
            </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>

        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Name of College 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
          
           <select  class="input100" required="" data-parsley-trigger="keyup"  name="col_id" id='col_id' value="<?php echo set_value('col_id'); ?>">
           <option value="0">Choose..</option>
          </select>
           
            <span class="focus-input100">
            </span>
          </div>
        </div>

        <?php echo form_error('col_id', '<div class="error">', '</div>'); ?>

		<div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Name of Student
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
           
            <input class="input100" type="text" required="" data-parsley-trigger="keyup"  name="name" id='name' placeholder="Name of Student"  value="<?php echo set_value('name'); ?>">
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <?php echo form_error('name', '<div class="error">', '</div>'); ?>

        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Email 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <input class="input100" type="email" data-parsley-type="email" required="" data-parsley-trigger="keyup" name="email" id='email' value="<?php echo set_value('email'); ?>" placeholder="Enter Email " >
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <?php echo form_error('email', '<div class="error">', '</div>'); ?>

        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Phone 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <input class="input100" type="text" required="" data-parsley-trigger="keyup" name="phone" id='phone' placeholder="Enter Phone " value="<?php echo set_value('phone'); ?>" data-parsley-maxlength="10" data-parsley-type="digits" >
            <span class="error focus-input100">
            </span>
          </div>
        </div>
        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Gender
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required=""  name="sex" id='sex' value="<?php echo set_value('sex'); ?>" >
            <option value="">Choose..</option>
              
              <option value="male">Male 
              </option>
              <option value="female">Female
              </option>
              <option value="Transgender">Transgender
              </option>
            </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Date of Birth 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <input class="input100" type="date" required="" data-parsley-trigger="keyup" name="dob" id='dob' value="<?php echo set_value('dob'); ?>" placeholder="" >
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Preferred Payment Mode 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required=""  name="payment" id='payment'  value="<?php echo set_value('payment'); ?>" >
            <option value="">Choose..</option>

              <option value="cash">Cash 
              </option>
              <option value="card">Card
              </option>
              <option value="net_banking">Net Banking 
              </option>
              <option value="paytm">PayTM 
              </option>
              <option value="gpay">GOogle Pay 
              </option>
              <option value="upi">UPI 
              </option>
            </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <div class="col-md-6">
          <label for="staticEmail" class=" col-form-label">Obtain for Scholarship 
          </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <select  class="input100" required="" name="scholarship" id='scholarship' value="<?php echo set_value('scholarship'); ?>" >
            
            <option value="">Choose..</option>

              <option value="Y">Yes 
              </option>
              <option value="N">No
              </option>
            </select>
            <span class="focus-input100">
            </span>
          </div>
        </div>
        <div class="col-md-12">
          <label for="staticEmail" class=" col-form-label">Message </label>
          <div class=" rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
            <textarea class="w-100" placeholder="Enter Massage"  name="message" value="<?php echo set_value('message'); ?>" ></textarea>
          </div>
        </div>

        <div class="col-md-12 text-center">
           <div class="g-recaptcha col-form-label" data-sitekey="6LeHo3kaAAAAAE5FUP6CnFUKU7WxO3urrDK0EByv"></div>
        </div>

        <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>">

        <div class="col-md-12">
         <div class="save_btn">
            <a href="<?=base_url();?>" class="c_n">Cancel
            </a>
            <input type="submit" id='form' class="btnContact" value="Submit" />
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>




<!-- start form vaildation using jquery -->

<style>
   input.parsley-error,
select.parsley-error,
textarea.parsley-error {    
    border-color:#D43F3A;
    box-shadow: none;
}

input.parsley-error:focus,
select.parsley-error:focus,
textarea.parsley-error:focus {    
    border-color:#D43F3A;
    box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #FF8F8A;
}

input.parsley-success,
select.parsley-success,
textarea.parsley-success {    
    border-color:#398439;
    box-shadow: none;
}

input.parsley-success:focus,
select.parsley-success:focus,
textarea.parsley-success:focus {    
    border-color:#398439;
    box-shadow:inset 0 1px 1px rgba(0,0,0,.075),0 0 6px #89D489
}

.parsley-errors-list {
    list-style-type: none;
    padding-left: 0;
    margin-top: 5px;
    margin-bottom: 0;
}

.parsley-errors-list.filled {
    color: #D43F3A;
    opacity: 1;
}  

</style>

<script type="text/javascript">
$(function () {
  $('#demo-form').parsley().on('field:validated', function() {
    var ok = $('.parsley-error').length === 0;
    $('.bs-callout-info').toggleClass('hidden', !ok);
    $('.bs-callout-warning').toggleClass('hidden', ok);
  })
  .on('form:submit', function() {
    return false; // Don't submit form for this demo
  });
});
    
    //------header-----
    $(document).ready(function(){
            $(".close").click(function(){

                $(".modal-backdrop.fade.show").hide();
                $(".show").hide();
                $(".modal-backdrop").hide();
            })
        })
    //------header-----
    
    
</script>

<!-- end form vaildation using jquery -->



<!-- Script -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

/* start Apply Now view college based upon university */


var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';

var csfr_token_value = '<?php echo $this->security->get_csrf_hash(); ?>';

$(document).ready(function(){
       
    
      $("#univ_id").change(function(){
            
       var data =  { univ_id : this.value, }

          data[csfr_token_name] = csfr_token_value;

            console.log(data);

            if ($("#univ_id").val().length == 0){

             alert('hello');
             // $("#univ_id").val("1");


            }

               $.ajax({
                  type: 'POST',
                  url: '<?php echo site_url('Apply_now/view_College'); ?>',
                  data: data,
                  success: function(obj){

                    console.log(obj);
                    $('#col_id').html(obj);

                        
                    // $.each(obj.result, function(i, item) {
                    //     alert(obj.result[i].col_name);
                    //     console.log(obj.result[i].col_name);
                    // })

                        }
               });
              
      });
      
   });


</script>

<script>
  $(function() {
    var timeout = 3000;
    $(".mass_age").delay(timeout).fadeOut(300);
  }
   );

   $("#w1").click(function(){
           $(".web1").show();
           $(".web2").hide();
       })   
              $("#w2").click(function(){
           $(".web1").hide();
           $(".web2").show();
       });
//    header
    
  $('.form-check-label').on('click', function(){
    $('.form-check-label.current').removeClass('current');
    $(this).addClass('current');
});
       
    
    
    
</script>
