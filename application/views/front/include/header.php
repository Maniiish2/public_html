<!DOCTYPE html>
<html lang="en-US">
  
   <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   
      <link rel="stylesheet" id="lp-course-wishlist-style-css" href="<?=base_url() ?>public/assets/css/wishlist40df.css?ver=5.6" type="text/css" media="all" />
      
      <link rel="stylesheet" id="layerslider-css" href="<?=base_url() ?>public/assets/css/layersliderce67.css" type="text/css" media="all" />
   
      <link rel="stylesheet"  href="<?=base_url() ?>public/assets/css/js_composer.min1e39.css"  />
      <link rel="stylesheet"  href="http://fonts.googleapis.com/cssfamily=Roboto%3A400%2C400i%2C500%2C500i%2C700%2C700i%26subset%3Dlatin%2Clatin-ext&amp;ver=4.2" />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/bootstrap.minbc19.css?ver=4.2"  />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/font-awesome.minbc19.css?ver=4.2"  />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/meanmenubc19.css?ver=4.2"  />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/defaultbc19.css?ver=4.2" />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/stylebc19.css?ver=4.2" />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/learnpressbc19.css?ver=4.2" />
      <link rel="stylesheet"  href=" <?=base_url() ?>public/assets/css/vcbc19.css?ver=4.2"  />
     <link rel='stylesheet'   href='<?=base_url() ?>public/assets/css/owl.carousel.minbc19.css?ver=4.2' type='text/css' media='all' />
    <link rel='stylesheet'   href='<?=base_url() ?>public/assets/css/owl.theme.default.minbc19.css?ver=4.2' type='text/css' media='all' />
    <link rel='stylesheet'   href='<?=base_url() ?>public/assets/css/course-review40df.css?ver=5.6' type='text/css' media='all' />

    <link rel='stylesheet'   href='<?=base_url() ?>public/assets/css/magnific-popup.minbc19.css?ver=4.2' type='text/css' media='all' />

    <link rel='stylesheet'   href='<?=base_url() ?>public/assets/css/default-skin.min287d.css' type='text/css' media='all' />


      <!-- need-->

<!--css-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  
  <script   src="https://code.jquery.com/jquery-1.12.3.min.js"   integrity="sha256-aaODHAgvwQW1bFOGXMeX+pC4PZIPsvn2h1sArYOhgXQ="   crossorigin="anonymous"></script>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link href="<?=base_url() ?>public/assets/css/style.css" rel="stylesheet">
<link href="<?=base_url() ?>public/assets/css/main.css" rel="stylesheet">
   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!--css-->
      
      
      
      <!-- start  plugins-->
      
      <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=6034c7d45b508c00110259c3&product=undefined' async='async'></script>
   
        <script type='text/javascript' src='https://app.crisp.chat/website/0d6a14ae-62da-462a-9fcc-3932d226a484/plugins/settings/c51aff87-114a-4a63-9281-094cb4c751c2/'></script>   
     <!-- end  plugins-->
      
     <style>

        .modal-content.info {
         width: 52%;
         margin-bottom: 29px;
         box-shadow: 0 0 0 10px rgb(255 255 255 / 38%);
         border-radius: 0;
     } 

   </style>
      
   </head>
   <body class="home page-template-default page page-id-54 wp-embed-responsive theme-eikra woocommerce-no-js header-style-1 has-topbar topbar-style-4 no-sidebar rt-course-grid-view product-grid-view wpb-js-composer js-comp-ver-6.4.2 vc_responsive elementor-default elementor-kit-1977">
      <div id="preloader" style="background-image: url( <?=base_url() ?>public/assets/img/preloader.gif);"></div>
      <div id="page" class="site">
         <!--     header-->
         <header id="masthead" class="site-header">
            <div id="tophead">
               <div class="container">
                  <div class="row">
                     <div class="col-sm-12">
                        <div class="tophead-contact">
                           <ul>
                              <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:+91 9051733245">+91 9051733245</a></li>
                              <li class="topbar-icon-seperator">|</li>
                              <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto:info@vivdista.com">info@vivdista.com</a></li>
                              <li class="topbar-icon-seperator">|</li>
                              <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="#footer">Contact</a></li>
                           </ul>
                        </div>
                        <div class="tophead-right">
                           <a href="#"><img src="<?=base_url() ?>public/assets/img/icon/facebook.png"></a>
                           <a href="#"><img src="<?=base_url() ?>public/assets/img/icon/twitter.png"></a>
                           <a href="#"><img src="<?=base_url() ?>public/assets/img/icon/insta.png"></a>
                           <a href="#"><img src="<?=base_url() ?>public/assets/img/icon/youtub.png"></a>
                           <a href="#"><img src="<?=base_url() ?>public/assets/img/icon/blog.png"></a>
                        </div>
                        <div class="clear"></div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container masthead-container">
               <div class="row">
                  <div class="col-sm-2 col-xs-12">
                     <div class="site-branding">
                        <a class="dark-logo" href="<?=base_url()?>"><img src=" <?=base_url() ?>public/assets/img/logo-dark.png" alt="vivdista" style="width: 56%;" /></a>
                        <a class="light-logo" href="<?=base_url()?>"><img src=" <?=base_url() ?>public/assets/img/logo-dark.png" alt="vivdista" /></a>
                     </div>
                  </div>
                  <div class="col-sm-10 col-xs-12">
                     <div class="header-icon-area">
                        <!-- <div class="additional-menu-area">
                           <div class="sidenav">
                              <a href="#" class="closebtn">x</a>
                              <ul id="menu-featured-links" class="menu">
                                 <li id="menu-item-67" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-67"><a href="#">Graduation</a></li>
                                 <li id="menu-item-68" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-68"><a href="#">Courses</a></li>
                                 <li id="menu-item-69" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-69"><a href="#">Admissions</a></li>
                                 <li id="menu-item-70" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-70"><a href="#">About Us</a></li>
                                 <li id="menu-item-71" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-71"><a href="#">International</a></li>
                                 <li id="menu-item-72" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-72"><a href="#">Book Store</a></li>
                                 <li id="menu-item-73" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-73"><a href="#">FAQs</a></li>
                                 <li id="menu-item-74" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-74"><a href="#">Alumni</a></li>
                              </ul>
                           </div>
                           <a class="side-menu-open side-menu-trigger"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        </div> -->
                        <div class="cart-icon-area">
                           <a href="<?=base_url('Home/auth')?>"><i class="fa fa-user user_icon">  </i><div class="seperator">|</div> </a>
   
                             
                        </div>
                        <div class="header-icon-seperator">|</div>
                        <div class="search-box-area">
                           <div class="search-box">
                              <form role="search" method="get" action="#">
                                 <a href="#" class="search-close">x</a>
                                 <input type="text" name="s" class="search-text" placeholder="Search Here..." required />
                                 <a href="#" class="search-button"><i class="fa fa-search" aria-hidden="true"></i></a>
                              </form>
                           </div>
                        </div>
                        <div class="clear"></div>
                     </div>
                     <div id="site-navigation" class="main-navigation">
                        <nav class="menu-main-menu-container">
                           <ul id="menu-main-menu" class="menu">
                              <li id="menu-item-57" class="mega-menu hide-header menu-item menu-item-type-custom menu-item-object-custom current-menu-ancestor menu-item-has-children menu-item-57">
                                 <a href="<?php echo site_url();?>">Home</a>
                              </li>
                             

                               <li id="menu-item-83" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-83">
                                 
                               
                                    <a class="btn dropdown-toggle" href="#About-us" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                       About Us
                                    </a>

<!--
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                       <a class="dropdown-item" href="#" id="myModal123">Our Commitment</a>
                                       <a class="dropdown-item" href="#" id="myModal124">Our Mission</a>
                                       <a class="dropdown-item" href="#" id="myModal125">Our Vision</a>
                                    </div>
-->
                                 <ul class="sub-menu">
                                <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-957"><a data-toggle="modal" data-target="#myModal1" href="#">Our Commitment</a></li>
                                <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-956"><a data-toggle="modal" data-target="#myModal2" href="#">Our Mission</a></li>
                                <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-955"><a data-toggle="modal" data-target="#myModal3" href="#">Our Vision</a></li>
                              
                            </ul>
                                 

                                 </li>
                              <li id="menu-item-82" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-82">
                                 <!-- <a href="#course.html">Our Courses</a> -->
                               
                                    <a class="btn dropdown-toggle" href="#About-us" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Our Courses
                                    </a>
                                    <ul class="sub-menu">
                                       <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-957"><a class="" data-toggle="modal" data-target="#course1" href="#">DigiSchoolSpace</a></li>
                                        <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-956"><a class="" data-toggle="modal" data-target="#course2" href="#">DigiCareerSpace</a></li>
                                    </ul>
                                  

                              </li>
                             
                              <li id="menu-item-82" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-82">
                              <a class="btn dropdown-toggle" href="#About-us" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Current Webinar
                                    </a>
                                    <ul class="sub-menu">
                                    <?php  if(!empty($view_Webinar)){
                                          foreach($view_Webinar as $vie_Webinar){
                                    ?>
                                       <li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-957"><a class="" ><?=$vie_Webinar->webinar;?></a></li>
                                       <?php } }?>
                                    </ul>
                              </li>
                              
                              <li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-85">
                                 <a href="#" id="myModal126">Why Vivdista</a>
                              </li>
                              <!-- <li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-85">
                                 <a href="#contnew.html">Contact</a>
                              </li> -->
                              
                              <li id="menu-item-85" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-85">
                                 <a class="topbar-btn apply" href="<?=base_url('Apply_now');?>">APPLY NOW</a>
                              </li>
                              
                              
                            
                           </ul>
                        </nav>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <div id="meanmenu" class=""></div>
         
         <!--     header-->


<!-- Course -->
<div class="modal fade" id="course1" role="dialog">

  <!-- Modal content -->
  <div class="modal-content info">
<!--    <span class="close" onclick="document.getElementById('myModal1').style.display='none'">&times;</span>-->
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2 class="our_commitment"><strong>DigiSchoolSpace</strong></h2>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Perform current state analysis of school students and mentor them based on Pareto Principle to bridge the gap</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Provide STEAM (Science, Technology, Engineering, Arts and Mathematics) education support through various unique initiatives and workshops</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> WProvide Life Skills education helping them to perform better in academic career and personal life</p>
  </div>

</div>


<div class="modal fade" id="course2" role="dialog">

  
  <div class="modal-content info">
<!--    <span class="close" onclick="document.getElementById('myModal1').style.display='none'">&times;</span>-->
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2 class="our_commitment"><strong>DigiCareerSpace</strong></h2>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Provide access to new-age technologies aligning the colleges and universities with Industry 4.0</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Provide Life Skills mentoring to build employability and employment sustainability skills</p>
         <div class="sub our_commitment_p">
             <ul>
                 <li>Foundation</li>
                 <li>Foundation+</li>
                 <li>Advanced+</li>
             </ul>
         </div>
          <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i>  Provide Entrepreneurship mentoring and guidance in terms of generation of ideas till Go To Market</p>
          <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i>Provide support to build Incubation Hub in the educational institution</p>
  </div>

</div>

<!-- Course -->
<!-- About -->
<div id="myModal1" role="dialog" class="modal fade">

  <!-- Modal content -->
  <div class="modal-content info">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2 class="our_commitment"><strong>Our Mission</strong></h2>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> To provide holistic and balanced growth of school as well as college students</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i>To keep students ready for their future, be it college readiness for a school students or employment/ entrepreneurship readiness for a college student</p>
  </div>

</div>


<div id="myModal2" role="dialog" class="modal fade">

  <!-- Modal content -->
  <div class="modal-content info">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2 class="our_commitment"><strong>Our Vision</strong></h2>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i>To be the most respected educational company of the country by 2030</p>
         
  </div>

</div>


<div id="myModal3" role="dialog" class="modal fade">

  <!-- Modal content -->
  <div class="modal-content info">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h2 class="our_commitment"><strong>Why Vivdista?</strong></h2>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> We understand the demand of 21st century education and our curriculum bridges the gap to meet that demand</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Our curriculums are dynamic and changes over time based on the changing demand of the industry</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> Our tutors have a unique blend of industry professional background and teaching experience and thus understand the need of the hour</p>
         <p class="our_commitment_p"><i class="fa fa-arrow-circle-right"></i> We are reasonably priced giving access to students from any background</p>
         
  </div>

</div>

<!-- About -->

<script>
    
    $(document).ready(function(){
        $(".close").click(function(){
     
           $(".modal-backdrop.fade.show").hide();
           $(".show").hide();
          
        })
    })
    


</script>

