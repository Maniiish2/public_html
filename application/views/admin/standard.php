<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.css"> 

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <section class="content">
    <!-- For Messages -->
    <?php $this->load->view('admin/includes/_messages.php') ?>
    <div class="card">
      <div class="card-header">
        <div class="d-inline-block">
          <h3 class="card-title"><i class="fa fa-list"></i>&nbsp; <?= 'standard List' ?></h3>
        </div>
        <div class="d-inline-block float-right">
           <a href="<?php //echo base_url('admin/Course/add_courses'); ?>" class="btn btn-success"><i class="fa fa-plus"></i> <?php //echo 'Add Course'?></a>
            </div>
      </div>
    </div>
    <div class="card">
      <div class="card-body table-responsive">
        <table id="na_datatable" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>
              <th>#<?= trans('id') ?></th>
              <th><?='standard name';?></th>

            </tr>
          </thead>
          <?php $i=1; foreach($result as $row){?>
          <tbody>
          <tr>
          
          <td><?=$i++?></td>
          <td><?=$row->standard_name?></td>
          </tr>
          </tbody>
          <?php }?>
        </table>
      </div>
    </div>
  </section>  
</div>


<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>

