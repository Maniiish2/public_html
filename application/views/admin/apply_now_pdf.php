<?php

$html = '
		<h3> Apply Now Form Users List</h3>
		<table border="1" style="width:100%">
			<thead>
				<tr class="headerrow">
					<th>Name</th>
					<th>Email</th>
					<th>Mobile Number</th>
					<th>Created Date</th>
				</tr>
			</thead>
			<tbody>';

			foreach($result as $row):
			$html .= '		
				<tr class="oddrow">
					<td>'.$row['name'].'</td>
					<td>'.$row['email'].'</td>
					<td>'.$row['phone'].'</td>
					<td>'.$row['submitted_at'].'</td>
				</tr>';
			endforeach;

			$html .=	'</tbody>
			</table>			
		 ';
				
				
			   
		$mpdf = new mPDF('c');

		$mpdf->SetProtection(array('print'));
		$mpdf->SetTitle("Vivdista - Apply Now Form Users List");
		$mpdf->SetAuthor("Vivdista");
		$mpdf->watermark_font = 'Vivdista';
		$mpdf->watermarkTextAlpha = 0.1;
		$mpdf->SetDisplayMode('fullpage');		 
		 

		$mpdf->WriteHTML($html);

		$filename = 'Apply_now_pdf_list';

		ob_clean();
		$mpdf->Output($filename . '.pdf', 'D');			

		exit;
