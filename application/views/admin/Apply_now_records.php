<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Apply_now_records extends MY_Controller {

	function __construct(){
		parent::__construct();
		auth_check(); // check login auth

		$this->rbac->check_module_access();
        $this->load->model('admin/Apply_now_model','admin_appy');
        

	}


    function index()
    {


    $data['results'] = $this->admin_appy->view();

            //       echo '<pre>';
            //       echo $data;
                 
            //       print_r($data);
            //       echo $this->db->last_query();

            //       echo '</pre>';

            //   exit;
     
        
        $this->load->view('admin/includes/_header');

        $this->load->view('admin/apply_now_view', $data);

        $this->load->view('admin/includes/_footer');

    }

    function standard()
    {

        $data['result']=$this->Apply_now_model->standard();

        $this->load->view('admin/includes/_header');

        $this->load->view('admin/standard', $data);

        $this->load->view('admin/includes/_footer');



    }
    function advanced_apply_now()
    {
        $this->session->unset_userdata('user_search_type');
		$this->session->unset_userdata('user_search_from');
		$this->session->unset_userdata('user_search_to');

		$data['title'] = 'Advanced Search with Datatable';

		$this->load->view('admin/includes/_header');
		$this->load->view('admin/examples/advance_search', $data);
		$this->load->view('admin/includes/_footer');


    }
    


}

?>
